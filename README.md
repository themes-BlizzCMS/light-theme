# Light Theme

* Theme for BlizzCMS-Plus.

Light theme based on the BlizzCMS-Plus Admin design.
All rights reserved to their respective owners.

https://wow-cms.com/en/ - https://gitlab.com/WoW-CMS

# How to get?

* Get this theme by contacting [Tyrael#4918](https://discord.com/) on Discord.

# Project

* https://wow-cms.com/en/
* https://gitlab.com/WoW-CMS

# Requirements

* BlizzCMS-Plus

# Screenshots

![Screenshot](Screenshot.png)
![Screenshot](Screenshot-1.png)
![Screenshot](Screenshot-2.png)
![Screenshot](Screenshot-3.png)
